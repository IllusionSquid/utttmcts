﻿using System;
using System.Diagnostics;
using System.Linq;

class Node
{
    public Node parent;
    public Node[] children;

    public (int bidx, int idx) move = (-1, -1);
    public int pid = 1;
    public int visitCount = 0;
    public int score = 0;
    public bool terminateState = false;
}

class UCT
{
    public static double uctValue(int totalVisit, double nodeWinScore, int nodeVisit)
    {
        if (nodeVisit == 0)
            return Int32.MaxValue;

        return (nodeWinScore / (double)nodeVisit) + 1.41 * Math.Sqrt(Math.Log(totalVisit) / (double)nodeVisit);
    }

    public static Node findBestNodeWithUCT(Node node)
    {
        int parentVisit = node.visitCount;
        return node.children.OrderByDescending(x => uctValue(parentVisit, x.score, x.visitCount)).First();
    }
}

class MonteCarloTreeSearch
{
    Stopwatch stopwatch = new Stopwatch();
    public UTTT copyUTTT = new UTTT(); // The UTTT class we will be abusing during simulation;
    Random rnd = new Random();

    public (int x, int y) FindBestMove(UTTT uttt, int time)
    {
        //int x = 0;
        stopwatch.Restart();

        // Copy current board into our abused UTTT
        uttt.boards.CopyTo(copyUTTT.boards, 0);
        uttt.board.CopyTo(copyUTTT.board, 0);
        copyUTTT.lastIDX = uttt.lastIDX;

        // Tree root creation
        Node root = new Node();
        root.move = (-1, uttt.lastIDX);

        while (stopwatch.ElapsedMilliseconds < time)
        {
            //x++;
            //Console.WriteLine(x);

            // Phase 1 - Selection
            //----------------------------------------------------------

            Node NodeToExplore = SelectPromisingNode(root);

            //Phase 1.5 - Board Correction
            //----------------------------------------------------------
            if (NodeToExplore.move.bidx != -1)
            {
                //Console.Error.WriteLine("Non root");
                Node temp = NodeToExplore.parent;
                while (temp.parent != null)
                {

                    //Console.Error.WriteLine("Non root");
                    copyUTTT.MoveInternal(temp.pid, temp.move.bidx, temp.move.idx);
                    temp = temp.parent;
                }

                copyUTTT.lastIDX = NodeToExplore.move.idx;
            }

            // Phase 2 - Expansion
            //----------------------------------------------------------
            //NodeToExplore.Expand();
            if (NodeToExplore.visitCount != 0 || NodeToExplore.move.bidx == -1)
            {
                copyUTTT.GetAvailableMovesBig();
                NodeToExplore.children = new Node[copyUTTT.NumberOfMoves];
                //copyUTTT.PrintBoard();

                int pid = NodeToExplore.pid ^ 0b1;
                // Create all moves for the nodes.
                for (int i = 0; i < copyUTTT.NumberOfMoves; i++)
                {
                    NodeToExplore.children[i] = new Node();
                    NodeToExplore.children[i].move = copyUTTT.moves[i];
                    NodeToExplore.children[i].pid = pid;
                    NodeToExplore.children[i].parent = NodeToExplore;
                }

                // Select the node for simulation
                if (NodeToExplore.children.Length > 0)
                {
                    NodeToExplore = GetRandomChildNode(NodeToExplore);
                }
            }

            // 

            // Phase 3 - Simulation
            //----------------------------------------------------------
            //int score = NodeToExplore.Simulate();

            // Play move of the node
            int status = copyUTTT.MoveInternal(NodeToExplore.pid, NodeToExplore.move.bidx, NodeToExplore.move.idx);
            int count = 0;
            // Random playout until terminate state
            if (status == 2)
            {
                (int s, int c) = copyUTTT.SimulateRandomPlayout(NodeToExplore.pid ^ 0b1);
                status = s;
                count = c;
            }

            // Phase 4 - Update
            //----------------------------------------------------------
            if (count == 0) // If this node is a finishing move
                NodeToExplore.terminateState = true;

            

            // Swapped 0 and 1 for player than we get 0 if no score and 1 if we win and 3 if we draw. 
            // (r & 0b1) * ((r % 3) + 1)
            // r = 0 = (0 & 0b1) * ((0 % 3) + 1) = 0 // Loss
            // r = 1 = (1 & 0b1) * ((1 % 3) + 1) = 2 // Win
            // r = 3 = (3 & 0b1) * ((3 % 3) + 1) = 1 // Draw
            int winScore = (status & 0b1) * ((status % 3) + 1);

            Node tempNode = NodeToExplore;

            tempNode.score += winScore;
            tempNode.visitCount++;

            int depth = 0;
            // Back Propagate
            while (tempNode.parent != null)
            {
                tempNode = tempNode.parent;
                tempNode.score += winScore;
                tempNode.visitCount++;
                depth++;
            }

            if (count == 0)
            {
                //Console.Error.WriteLine($"This node is a terminate state {NodeToExplore.move} {depth}");
                if (depth == 1 && winScore == 2)
                {
                    //Console.Error.WriteLine($"A winning move {NodeToExplore.move}");
                    NodeToExplore.score += 999999;
                }
                else if (depth == 2 && winScore == 0)
                {
                    //Console.Error.WriteLine($"LosingMove {NodeToExplore.move}");
                    NodeToExplore.score -= 999999;
                    NodeToExplore.parent.score -= 999999;
                }
            }

            // Reset Board
            //----------------------------------------------------------
            uttt.boards.CopyTo(copyUTTT.boards, 0);
            uttt.board.CopyTo(copyUTTT.board, 0);
            copyUTTT.lastIDX = uttt.lastIDX;
        }

        // Get winner (Child with highest score)
        Node winner = root.children[0];
        for (int i = 1; i < root.children.Length; i++)
        {
            if (root.children[i].score > winner.score)
            {
                winner = root.children[i];
            }
        }


        // Return winner

        //Console.Error.WriteLine(winner.move);
        int bCol = winner.move.bidx % 3;
        //int bRow = (move.bidx - bCol) / 3;
        int sCol = winner.move.idx % 3;
        //int sRow = (move.idx - sCol) / 3;
        //moves[NumberOfMoves] = (sRow + bRow * 3, sCol + bCol * 3);
        //move = (sRow + bRow * 3, sCol + bCol * 3);
        //move = (((move.idx - sCol) / 3) + (move.bidx - bCol), sCol + bCol * 3);

        //Console.Error.WriteLine($"Iterations: {x}");
        
        return (((winner.move.idx - sCol) / 3) + (winner.move.bidx - bCol), sCol + bCol * 3);
    }

    Node SelectPromisingNode(Node root)
    {
        Node node = root;
        while (node.children != null)
        {
            if (node.children.Length == 0)
                break;

            node = UCT.findBestNodeWithUCT(node);
        }

        return node;
    }

    Node GetRandomChildNode(Node ExploreNode)
    {
        //int index = rnd.Next(ExploreNode.children.Length);
        return ExploreNode.children[rnd.Next(ExploreNode.children.Length)];
    }
}

class UTTT
{
    public int[] boards = new int[18] {
        // Player big board
        0b0, 0b0, 0b0,
        0b0, 0b0, 0b0,
        0b0, 0b0, 0b0,
        // Opponend big board
        0b0, 0b0, 0b0,
        0b0, 0b0, 0b0,
        0b0, 0b0, 0b0,
    };

    public int[] board = new int[2] { 0b0, 0b0 };

    public static int[] masks = new int[8]
    {
        0b000_000_111,
        0b000_111_000,
        0b111_000_000,

        0b001_001_001,
        0b010_010_010,
        0b100_100_100,

        0b100_010_001,
        0b001_010_100
    };

    public (int bidx, int idx)[] moves = {
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1),
        (-1, -1), (-1, -1), (-1, -1)
    };

    public int NumberOfMoves;

    public int lastIDX = -1;

    Random rnd = new Random();

    //Stopwatch stopwatch = new Stopwatch();

    public UTTT()
    {

    }

    public UTTT(UTTT uttt)
    {
        uttt.boards.CopyTo(boards, 0);
        uttt.board.CopyTo(board, 0);
    }

    public static int IsWin(int board)
    {
        for (int i = 0; i < 8; i++)
        {
            if ((board & masks[i]) == masks[i])
            {
                return 1;
            }
        }
        return 0;
    }

    public int GetAvailableBoards()
    {
        return 0b111_111_111 ^ (board[0] | board[1]);
    }

    public void GetAvailableMovesBig()
    {
        NumberOfMoves = 0;
        if (lastIDX >= 0)
        {
            if ((GetAvailableBoards() & (0b1 << lastIDX)) != 0)
            {
                // We can only play on this small board
                int available = GetAvailableMovesSmall(lastIDX);

                for (int j = 0; j < 9; j++)
                {
                    if ((available & (0b1 << j)) != 0)
                    {
                        moves[NumberOfMoves] = (lastIDX, j);
                        NumberOfMoves++;
                    }
                }
                return;
            }
        }

        int b = GetAvailableBoards();
        // We can play everywhere
        for (int i = 0; i < 9; i++)
        {
            //Console.Error.WriteLine(Convert.ToString(b, toBase: 2));
            if ((b & (0b1 << i)) != 0)
            {
                int available = GetAvailableMovesSmall(i);

                for (int j = 0; j < 9; j++)
                {
                    if ((available & (0b1 << j)) != 0)
                    {
                        moves[NumberOfMoves] = (i, j);
                        NumberOfMoves++;
                    }
                }
            }
        }

        return;
    }

    public unsafe int CalcWinStateBig(int playerId)
    {
        bool A = GetAvailableBoards() == 0;
        int w = IsWin(board[playerId]);
        int r = w ^ 0b1;


        // Returns the following:
        // Undecided = 2
        // Draw = 3
        // Win = 1 - playerId
        // NOTE: In a case of a win we reverse the playerId unlike in CalcWinStateSmall where we don't
        return (w * (playerId ^ 0b1) + r * *(byte*)(&A) + r * 2);
    }

    int MoveBig(int playerId, int idx)
    {
        board[playerId] = board[playerId] | (0b1 << idx);
        return CalcWinStateBig(playerId);
    }

    int GetAvailableMovesSmall(int boardId)
    {
        return 0b111_111_111 ^ (boards[boardId] | boards[boardId + 9]);
    }

    unsafe int CalcWinStateSmall(int playerId, int boardId)
    {
        bool A = GetAvailableMovesSmall(boardId) == 0;
        int w = IsWin(boards[playerId * 9 + boardId]);
        int r = w ^ 0b1;

        // Returns the following:
        // Undecided = 2
        // Draw = 3
        // Win = playerId
        return (w * playerId + r * *(byte*)(&A) + r * 2);
    }

    public int MoveSmall(int playerId, int boardId, int idx)
    {
        boards[playerId * 9 + boardId] |= (0b1 << idx);
        return CalcWinStateSmall(playerId, boardId);
    }

    public int Move(int playerID, int row, int col)
    {
        // Way too big calculation to convert 0 - 9 coords to my data structure.
        int smallRow = row % 3;
        int smallCol = col % 3;
        int bigRow = (row - smallRow) / 3;
        int bigCol = (col - smallCol) / 3;
        lastIDX = smallRow * 3 + smallCol;
        int bidx = bigRow * 3 + bigCol;

        if (MoveSmall(playerID, bidx, lastIDX) != 2)
        {
            return MoveBig(playerID, bidx);
        }
        else
        {
            return 2;
        }
    }

    public int MoveInternal(int playerID, int bidx, int idx)
    {
        // Note that here we are already working with the converted coords.
        // Just so we don't constantly convert back and forth between them.

        lastIDX = idx;

        if (MoveSmall(playerID, bidx, lastIDX) != 2)
        {
            return MoveBig(playerID, bidx);
        }
        else
        {
            return 2;
        }
    }

    public int PlayRandomMove(int playerId)
    {
        GetAvailableMovesBig();
        if (NumberOfMoves > 0)
        {
            int index = rnd.Next(NumberOfMoves);
            (int bidx, int idx) move = moves[index];

            return MoveInternal(playerId, move.bidx, move.idx);
        }
        else
        {
            return 3;
        }
    }

    public (int result, int moveCount) SimulateRandomPlayout(int playerId)
    {
        int status = 2;
        int count = 0;

        while (status == 2)
        {
            playerId ^= 0b1;
            status = PlayRandomMove(playerId);
            count++;
        }

        return (status, count);
    }

    public void PrintBoard()
    {
        string[] buf = new string[9] { "0 ", "1 ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 " };

        GetAvailableMovesBig();

        for (int row = 0; row < 3; row++)
        {
            for (int sRow = 0; sRow < 3; sRow++)
            {
                for (int col = 0; col < 3; col++)
                {
                    for (int sCol = 0; sCol < 3; sCol++)
                    {
                        int bidx = col + row * 3;
                        int idx = sCol + sRow * 3;
                        string x = $"{bidx}, {idx}";

                        int b0 = boards[bidx];
                        int b1 = boards[9 + bidx];
                        int tmp = 0b1 << idx;
                        if (((b0 | b1) & tmp) == 0)
                        {
                            if (moves.Take(NumberOfMoves).Contains((bidx, idx)))
                            {
                                x = ".";
                            }
                            else
                            {
                                x = " ";
                            }
                        }
                        else if ((b0 & tmp) != 0)
                        {
                            x = "X";
                        }
                        else if ((b1 & tmp) != 0)
                        {
                            x = "O";
                        }
                        buf[row * 3 + sRow] += x;
                    }
                    if (col < 2)
                        buf[row * 3 + sRow] += "|";
                }
            }
        }


        Console.Error.WriteLine("  012 345 678");
        for (int i = 0; i < buf.Length; i++)
        {
            if (i == 3 || i == 6)
            {
                Console.Error.WriteLine("  ---+---+---");
            }

            Console.Error.WriteLine(buf[i]);
        }
    }
}

class Player
{
    unsafe static void Main(string[] args)
    {
        //PlayCodinGame();

        Play();


        //int board = 0b000_010_011;
        //board = board << 2;
        //SmallBoard sBoard = new SmallBoard();
        //sBoard.player[0] = board;
        //Console.Error.WriteLine(Convert.ToString(sBoard.getAvailableMoves(), toBase: 2));
        //Console.Error.WriteLine($"{2 * 3 + 2}");

        //UTTT uttt = new UTTT();

        //Console.WriteLine(uttt.Move(0, 1, 4));
        //bool A = 0b1 == 0;
        ////Console.WriteLine(*(byte*)(&A));
        //int w = 1;
        //int p = 1;
        //Console.WriteLine(((w) * (p)) + (~w & 0b1) * *(byte*)(&A) + (~w & 0b1) * 2);

        //((int x, int y)[] moves, int count) moves = uttt.GetAvailableMovesBig();
        //Console.WriteLine("{0}", string.Join(", \n", moves.moves));
        //while (true)
        //{
        //    Console.Error.WriteLine(Convert.ToString(uttt.board[0], toBase: 2));
        //    Console.Error.WriteLine(Convert.ToString(uttt.boards[0, 0], toBase: 2));
        //    string[] inputs = Console.ReadLine().Split(' ');
        //    int opponentRow = int.Parse(inputs[0]);
        //    int opponentCol = int.Parse(inputs[1]);

        //    uttt.PlayRandomMove(0);
        //    Console.Error.WriteLine(Convert.ToString(uttt.board[0], toBase: 2));
        //    Console.Error.WriteLine(Convert.ToString(uttt.boards[0, 0], toBase: 2));
        //    ((int x, int y)[] moves, int count) moves = uttt.GetAvailableMovesBig();
        //    Console.WriteLine("{0}", string.Join(", \n", moves.moves));
        //}

        //Console.WriteLine(uttt.SimulateRandomPlayout(0, (0, 0)));
        //for (int i = 0; i < 9; i++)
        //{
        //    Console.Error.WriteLine(Convert.ToString(uttt.boards[0, i], toBase: 2));
        //}

        //uttt.PrintBoard();
        //MonteCarloTreeSearch MCTS = new MonteCarloTreeSearch();
        //Console.WriteLine(MCTS.FindBestMove(uttt, 80));
        //(int x, int y) move = MCTS.FindBestMove(uttt, 80);
        //Console.Error.WriteLine("___________");
        //MCTS.FindBestMove(uttt, 80);
        //for (int i = 0; i < 81; i++)
        //{
        //    Console.WriteLine((((~i + 1) >> 31) & 1) ^0b1);

        //}
        //Console.WriteLine((0 & 0b1) * ((0 % 3) + 1));
        //Console.WriteLine((1 & 0b1) * ((1 % 3) + 1));
        //Console.WriteLine((3 & 0b1) * ((3 % 3) + 1));

        //bool A = 3 == 0;
        ////Console.WriteLine(*(byte*)(&A));
        //int w = 0;
        //int r = w ^ 0b1;
        //Console.WriteLine(w * (1 ^ 0b1) + r * *(byte*)(&A) + r * 2);
        //Console.WriteLine(1 ^ 0b1);

        //Console.ReadKey();
    }

    public static void Play()
    {
        string[] inputs;

        // Initialize objects;
        UTTT uttt = new UTTT();
        MonteCarloTreeSearch MCTS = new MonteCarloTreeSearch();

        uttt.Move(1, 0, 0);
        while (true)
        {
            (int x, int y) move = MCTS.FindBestMove(uttt, 80);
            uttt.Move(0, move.x, move.y);
            Console.WriteLine($"{move.x} {move.y}");

            uttt.PrintBoard();

            inputs = Console.ReadLine().Split(' ');
            int opponentRow = int.Parse(inputs[0]);
            int opponentCol = int.Parse(inputs[1]);

            uttt.Move(1, opponentRow, opponentCol);
        }

        Console.ReadKey();
    }

    public static void PlaySelf()
    {
        string[] inputs;

        // Initialize objects;
        UTTT uttt = new UTTT();
        UTTT uttt2 = new UTTT();
        MonteCarloTreeSearch MCTS = new MonteCarloTreeSearch();
        MonteCarloTreeSearch MCTS2 = new MonteCarloTreeSearch();

        while (true)
        {
            (int x, int y) move = MCTS.FindBestMove(uttt, 80);
            uttt.Move(0, move.x, move.y);
            uttt2.Move(1, move.x, move.y);
            //Console.WriteLine($"{move.x} {move.y}");

            //uttt.PrintBoard();
            move = MCTS2.FindBestMove(uttt2, 80);
            uttt2.Move(0, move.x, move.y);
            uttt.Move(1, move.x, move.y);
        }

        uttt.PrintBoard();
        Console.ReadKey();
    }

    public static void PlayCodinGame()
    {
        string[] inputs;

        // Initialize objects;
        UTTT uttt = new UTTT(); 
        MonteCarloTreeSearch MCTS = new MonteCarloTreeSearch();


        while (true)
        {
            // Parse opponend inputs;
            inputs = Console.ReadLine().Split(' ');
            int opponentRow = int.Parse(inputs[0]);
            int opponentCol = int.Parse(inputs[1]);


            if (opponentRow != -1 && opponentCol != -1)
            {
                uttt.Move(1, opponentRow, opponentCol);
            }

            // Parse valid actions that will never be used.....
            int validActionCount = int.Parse(Console.ReadLine());
            int[,] validPlaces = new int[validActionCount, 2];
            for (int i = 0; i < validActionCount; i++)
            {
                inputs = Console.ReadLine().Split(' ');
                validPlaces[i, 0] = int.Parse(inputs[0]);
                validPlaces[i, 1] = int.Parse(inputs[1]);
            }

            // Perform MCTS
            (int x, int y) move = MCTS.FindBestMove(uttt, 80);

            // Place Move
            uttt.Move(0, move.x, move.y);
            Console.WriteLine($"{move.x} {move.y}");
        }
    }
}