﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace UTTT_MCTS
{
    [TestFixture]
    class UTTTTest
    {

        [Test]
        public void GetAvailableBoardsTest()
        {
            UTTT uttt = new UTTT();

            Assert.AreEqual(0b111_111_111, uttt.GetAvailableBoards());

            uttt.board[0] = 0b000_000_001;
            Assert.AreEqual(0b111_111_110, uttt.GetAvailableBoards());
            uttt.board[1] = 0b100_000_000;
            Assert.AreEqual(0b011_111_110, uttt.GetAvailableBoards());
        }

        [Test]
        public void MoveSmallTest()
        {
            UTTT uttt = new UTTT();

            uttt.MoveSmall(0, 0, 0);
            Assert.AreEqual(0b000_000_001, uttt.boards[0]);
            uttt.MoveSmall(0, 0, 8);
            Assert.AreEqual(0b100_000_001, uttt.boards[0]);
        }

        [Test]
        public void MoveTest()
        {
            UTTT uttt = new UTTT();

            uttt.Move(0, 0, 0);
            uttt.Move(0, 1, 1);
            uttt.Move(0, 2, 2);
            Assert.AreEqual(0b000_000_001, uttt.board[0]);

        }

        [Test]
        public void IsWinTest()
        {
            //UTTT uttt = new UTTT();

            for (int i = 0; i < UTTT.masks.Length; i++)
            {
                Assert.AreEqual(1, UTTT.IsWin(UTTT.masks[i]));
            }
            Assert.AreEqual(0, UTTT.IsWin(0b000_000_000));
            Assert.AreEqual(0, UTTT.IsWin(0b001_000_000));
        }

        [Test]
        public void GetAvailableMovesBigTest()
        {
            UTTT uttt = new UTTT();

            uttt.GetAvailableMovesBig();
            Assert.AreEqual(81, uttt.NumberOfMoves);
            uttt.Move(0, 2, 2);
            uttt.GetAvailableMovesBig();
            Assert.AreEqual(9, uttt.NumberOfMoves);
            uttt.Move(0, 1, 1);
            uttt.Move(0, 0, 0);
            uttt.PrintBoard();

            Assert.AreEqual(72, uttt.NumberOfMoves);
        }

        [Test]
        public void BoardCorrectionTest()
        {
            UTTT uttt = new UTTT();

            Node root = new Node();

            int playerId = 1;

            uttt.GetAvailableMovesBig();
            root.children = new Node[uttt.NumberOfMoves];

            // Create all moves for the nodes.
            for (int i = 0; i < uttt.NumberOfMoves; i++)
            {
                root.children[i] = new Node();
                root.children[i].parent = root;
                root.children[i].move = uttt.moves[i];
            }

            uttt.MoveInternal(0, root.children[0].move.bidx, root.children[0].move.idx);

            uttt.GetAvailableMovesBig();
            root.children[0].children = new Node[uttt.NumberOfMoves];

            // Create all moves for the nodes.
            for (int i = 0; i < uttt.NumberOfMoves; i++)
            {
                root.children[0].children[i] = new Node();
                root.children[0].children[i].parent = root.children[0];
                root.children[0].children[i].move = uttt.moves[i];
            }

            uttt.MoveInternal(1, root.children[0].children[0].move.bidx, root.children[0].children[0].move.idx);

            UTTT copyUTTT = new UTTT();

            Node temp = root.children[0].children[0];
            while (temp.parent != null)
            {
                copyUTTT.MoveInternal(playerId, temp.move.bidx, temp.move.idx);
                playerId ^= 0b1;
                temp = temp.parent;
            }

            uttt.PrintBoard();
            copyUTTT.PrintBoard();


            for (int i = 0; i < 18; i++)
            {
                Assert.AreEqual(uttt.boards[i], copyUTTT.boards[i]);
            }
        }

        [Test]
        public void CalcWinStateBigTest()
        {
            UTTT uttt = new UTTT();

            Assert.AreEqual(2, uttt.CalcWinStateBig(0));

            uttt.boards = new int[18] {
                // Player big board
                0b0, 0b0, 0b0,
                0b0, 0b0, 0b0,
                0b111_111_111, 0b111_111_111, 0b111_111_111,
                // Opponend big board
                0b0, 0b0, 0b0,
                0b0, 0b0, 0b0,
                0b0, 0b0, 0b0,
            };
            uttt.board = new int[2] { 0b111_000_000, 0b0 };
            Assert.AreEqual(1, uttt.CalcWinStateBig(0));

            uttt.boards = new int[18] {
                // Player big board
                0b0, 0b0, 0b0,
                0b0, 0b0, 0b0,
                0b0, 0b0, 0b0,
                // Opponend big board
                0b0, 0b0, 0b0,
                0b0, 0b0, 0b0,
                0b111_111_111, 0b111_111_111, 0b111_111_111
            };
            uttt.board = new int[2] { 0b0, 0b111_000_000 };
            Assert.AreEqual(0, uttt.CalcWinStateBig(1));
        }
    }
}
